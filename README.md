# Debtags tag vocabulary

This repository contains the tag vocabulary used by debtags and https://debtags.debian.org

Pushing to master, if the CI tests pass, triggers an automatic update of tags
on debtags.debian.org.

The vocabulary is in the file `debian-packages`. You can edit it and use the
`test-vocabulary` script to validate it before a push.

See [here](https://wiki.debian.org/Debtags/FAQ#What_makes_a_tag_good_for_being_added_to_the_vocabulary.3F)
for a rough guideline for adding new tags.
